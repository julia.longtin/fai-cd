#!/bin/sh

GRUB_DEFAULT=etc/default/grub

# Enable console mode for the grub menu:
$ROOTCMD sed -i 's/#\(GRUB_TERMINAL=console\)/\1/' $GRUB_DEFAULT

# Update /boot
$ROOTCMD update-grub

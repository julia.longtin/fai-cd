#!/bin/sh

# Copy the install script
fcopy /etc/init.d/install_drupal

# Register it with the init system
$ROOTCMD insserv -d install_drupal
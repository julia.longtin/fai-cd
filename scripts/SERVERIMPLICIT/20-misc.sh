#!/bin/bash

# create a directory for containing the ruby codebase.
mkdir -p /home/implicit/implicitcad.org
$(ROOTCMD) chown -R implicit /home/implicit/implicitcad.org

# disable the default profile, switching to the configuration for proxying to the implicitcad website.
$(ROOTCMD) a2dissite 000-default
$(ROOTCMD) a2ensite 001-implicitserver

# copy the post install script, for performing steps after installation.
fcopy -Bv root/post_install_implicitserver.sh

# copy the shell script used by systemd to start the implicitcad ruby on rails app.
fcopy -Bv usr/local/bin/implicitcad_website.sh
$(ROOTCMD) chmod 755 /usr/local/bin/implicitcad_website.sh

# copy the systemd service definition.
fcopy -Bv etc/systemd/system/implicitcad_website.service

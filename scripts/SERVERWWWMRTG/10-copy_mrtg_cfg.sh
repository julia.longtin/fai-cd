#!/bin/sh

fcopy etc/mrtg.cfg
fcopy etc/mrtg/ping_internet.sh
fcopy etc/mrtg/ping_target.sh
fcopy etc/mrtg/show_acpifan.sh
fcopy etc/mrtg/show_acpitemp.sh
fcopy etc/mrtg/show_cpus.sh
fcopy etc/mrtg/show_disk_io_for_container_disk.sh
fcopy etc/mrtg/show_hddio.sh
fcopy etc/mrtg/show_hddtemp.sh
fcopy etc/mrtg/show_k8temp.sh
fcopy etc/mrtg/show_temperature_for_container_disk.sh
fcopy etc/mrtg/show_voltage.sh
$ROOTCMD bash -c "su nfrg -c \"mkdir -p /etc/mrtg/mrtg.d\""
$ROOTCMD bash -c "su nfrg -c \"mkdir -p /etc/mrtg/hosts-enabled/\""

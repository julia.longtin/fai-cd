#! /bin/sh

# let people on the local network relay mail through this server

## Our "die" function (think perl)
function die () { echo "$@" 1>&2 ; exit 1 ; }

$ROOTCMD bash -c "echo 'MAIN_TLS_ENABLE = true' >> /etc/exim4/conf.d/main/00_local_settings"


#! /bin/sh

# let people on the local network relay mail through this server

## Our "die" function (think perl)
function die () { echo "$@" 1>&2 ; exit 1 ; }

fcopy -Bv /etc/exim4/conf.d/auth/40-auth_plain_if_tls


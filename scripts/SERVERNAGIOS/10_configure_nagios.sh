#! /bin/sh

# Set up default admin user
$ROOTCMD htpasswd -bc "${NAGIOS_CFG}"/htpasswd.users "${NAGIOSADMIN}" "${NAGIOSADMINPW}"
sed -i $target/"${NAGIOS_CFG}"/cgi.cfg 's/nagiosadmin/"${NAGIOSADMIN}/g'

# Nuke the old config directory from `orbit'
rm -rf $target/"${NAGIOS_CFG}"/conf.d/*

fcopy -Bvr "${NAGIOS_CFG}"/conf.d

# Add the admin scripts
fcopy -Bv /root/add_nagios_user.sh
fcopy -Bv /root/sync_to_qemuhost.sh
fcopy -Bv /root/add_nagios_monitor.sh
fcopy -Brv /usr/share/cm_nagios

# Add cronjob for nagios updates.
$ROOTCMD (echo '*/5 * * * * /usr/share/cm_nagios/cron/cm_nagios_update_add.sh' | crontab -)

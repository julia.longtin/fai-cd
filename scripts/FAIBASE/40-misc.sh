#!/bin/bash
# 40-misc.sh
#
# drop-in replacement for 40-misc cfengine script. cfengine sucks.

## Our "die" function (think perl)
function die () { echo "$@" 1>&2 ; exit 1 ; }

[ ! -n "$target" ] && die "variable \$target not defined!"
[ ! -n "$HOSTNAME" ] && die "variable \$HOSTNAME not defined!"
[ ! -n "$ROOTCMD" ] && die "variable \$ROOTCMD not defined!"
[ ! -n "$UTC" ] && die "variable \$UTC not defined!"

# FIXME: shouldnt these match the settings on ichi, meaning nobody/nogroup/755?
$ROOTCMD chown 0:0 /tmp
$ROOTCMD chmod 1777 /tmp

# NOTE: this greps for the hostname, so lines like "hostname.subdomain.com" are also noticed. different than origional cfengine script.
if [ -f $target/etc/mailname ]; then
    {
	if [ ! -n "`grep \"$HOSTNAME\" $target/etc/mailname`" ]; then
	    echo "$HOSTNAME" >> $target/etc/mailname
	fi
    }
else
    echo "$HOSTNAME" >> $target/etc/mailname
fi

if [ -f $target/etc/default/rcS ]; then
    {
	cat $target/etc/default/rcS | sed "s/^UTC=.*/UTC=$UTC/" > /tmp/jtemp
	cat /tmp/jtemp > $target/etc/default/rcS
    }
else die "$target/etc/default/rcS not found!"
fi

if [ -f $target/etc/hosts ]; then
    {
	if [ ! -n "`grep \"127.0.0.1\" $target/etc/hosts`" ]; then
	    echo $'127.0.0.1\tlocalhost\t$HOSTNAME' >> $target/etc/hosts
	fi
    }
else die "$target/etc/hosts not found!"
fi

#for old sysv init systems.
if [ -f $target/etc/inittab ]; then
    {
	cat $target/etc/inittab | sed "s=getty 38400=getty -f /etc/issue.linuxlogo 38400=" > /tmp/jtemp
	cat /tmp/jtemp > $target/etc/inittab
    }
fi

# for systemd systems.
if [ -f $target/lib/systemd/system/getty\@.service ]; then
    {
	cat $target/lib/systemd/system/getty\@.service | sed "s=getty --noclear=getty -f /etc/issue.linuxlogo --noclear=" > /tmp/jtemp
	cat /tmp/jtemp > $target/lib/systemd/system/getty\@.service
    }
fi

if [ ! -f $target/etc/inittab ]; then
    {
	if [ ! -f $target/lib/systemd/system/getty\@.service ]; then
	    {
		die "could not set up linuxlogo."
	    }
	fi
    }
fi
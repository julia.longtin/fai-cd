#! /bin/sh

# re-generate /etc/exim4/update-exim4.conf.conf
# if you dont remove the file, it will read it, and override debconf.
$ROOTCMD bash -c "rm /etc/exim4/update-exim4.conf.conf"
$ROOTCMD bash -c "dpkg-reconfigure exim4-config -f noninteractive"

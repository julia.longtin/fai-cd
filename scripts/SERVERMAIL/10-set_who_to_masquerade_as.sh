#! /bin/bash

error=0 ; trap "error=$((error|1))" ERR

# set the name returned by our mailserver, EG the dns name people are using to access this mailserver. usually a DNS name pointing to the firewall forwarding packets to this machine.

# check to make sure the expected variables have been declared
[ -z "$HOSTNAME" ] && { echo "ERROR: HOSTNAME NOT FOUND!" 1>&2; exit 1}
[ -z "$PRIMARY_DOMAIN_DNS" ] && { echo "ERROR: PRIMARY_DOMAIN_DNS NOT FOUND!" 1>&2; exit 1}

# set the system mailname, domainname, and debconf entries to indicate THIS is the primary domain of the system.

$ROOTCMD bash -c "echo 'exim4-config    exim4/dc_other_hostnames        string  $PRIMARY_DOMAIN_DNS' | debconf-set-selections"
$ROOTCMD bash -c "echo 'exim4-config    exim4/mailname                  string  $PRIMARY_DOMAIN_DNS' | debconf-set-selections"

$ROOTCMD bash -c "echo '$PRIMARY_DOMAIN_DNS' > /etc/mailname"
$ROOTCMD bash -c "echo '$HOSTNAME.$PRIMARY_DOMAIN_DNS' > /etc/hostname"

if [ -f $target/etc/exim4/conf.d/main/01_exim4-config_listmacrosdefs ]; then
    {
        if [ ! -n "`grep \"^smtp_banner\" $target/etc/exim4/conf.d/main/01_exim4-config_listmacrosdefs`" ]; then
            echo "smtp_banner= \"$PRIMARY_DOMAIN_DNS ESMTP Exim \$version_number \$tod_full\"" >> $target/etc/exim4/conf.d/main/01_exim4-config_listmacrosdefs
        fi
    }
else
    {
	echo "$target/etc/exim4/conf.d/main/01_exim4-config_listmacrosdefs not found." 1>&2
	exit 1;
    }
fi

exit $error
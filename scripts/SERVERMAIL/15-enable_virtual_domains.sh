#! /bin/sh

# tell exim4 to use /etc/exim4/virtual for virtual domain alias files.
# this is a real round-about hack.

# check to make sure our variables have been declared
[ ! -n "$PRIMARY_DOMAIN_DNS" ] && { echo "ERROR: PRIMARY_DOMAIN_DNS NOT FOUND!" ; exit 1; }

$ROOTCMD bash -c "echo 'exim4-config    exim4/dc_other_hostnames        string  $PRIMARY_DOMAIN_DNS:dsearch;/etc/exim4/virtual' | debconf-set-selections"

# and the router..
fcopy etc/exim4/conf.d/router/350_exim4-config_vdom_aliases

# don't forget to make the directory...
$ROOTCMD mkdir /etc/exim4/virtual
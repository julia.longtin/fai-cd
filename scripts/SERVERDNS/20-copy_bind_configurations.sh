#!/bin/sh
function die ()
{
    echo ERROR: $1 NOT FOUND
    exit 1
}

[ -z "$target" ] && die target

# our "main" bind configuration
fcopy /etc/bind/named.conf.local

# this file should be replaced with one that mentions our upstream dns server
fcopy /etc/bind/named.conf.options

# the zone file containing the root nameservers.
fcopy /var/cache/bind/named.ca

#!/bin/sh
function die ()
{
    echo ERROR: $1 NOT FOUND
    exit 1
}

[ -z "$target" ] && die target

# these are jumping targets. fcopy, then destroy.

fcopy /var/cache/bind/PRIMARY_DOMAIN_EXTERNAL.zone 
rm -rf $target/var/cache/bind/PRIMARY_DOMAIN_EXTERNAL.zone

fcopy /var/cache/bind/PRIMARY_DOMAIN_INTERNAL.zone
rm -rf $target/var/cache/bind/PRIMARY_DOMAIN_INTERNAL.zone

fcopy /var/cache/bind/PRIMARY_DOMAIN_INTERNAL_REVERSE.zone
rm -rf $target/var/cache/bind/PRIMARY_DOMAIN_INTERNAL_REVERSE.zone

fcopy /var/cache/bind/PRIMARY_DOMAIN_LOCAL.zone
rm -rf $target/var/cache/bind/PRIMARY_DOMAIN_LOCAL.zone


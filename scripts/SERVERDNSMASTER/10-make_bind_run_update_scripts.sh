#!/bin/bash
# 10-make_bind_run_update_scripts.sh
#
# drop-in replacement for 10-make_bind_run_update_scripts script.

## Our "die" function
function die () { echo "$@" 1>&2 ; exit 1 ; }

[ ! -n "$target" ] && die "variable \$target not defined!"

if [ -f $target/etc/init.d/bind9 ]; then
    {
        if [ -n "`cat $target/etc/init.d/bind9| sed -n \"/.*echo .* \/sbin\/resolv.conf .*/{n;p;}\"`" ]; then
            if [ "`cat $target/etc/init.d/bind9| sed -n \"/.*echo .* \/sbin\/resolv.conf .*/{n;p;}\"`" != "# cfengine - scripts/SERVERDNS/40-make_bind_run_update_scripts" ]; then
                {
                    cat $target/etc/init.d/bind9| sed "s/^\(.*\)echo \(.*\) \/sbin\/resolv.conf \(.*\)$/\1echo \2 \/sbin\/resolv.conf \3\n# cfengine - scripts/SERVERDNS/40-make_bind_run_update_scripts\n            if [ -x \/root\/scripts\/SERVERDNS\/update_localnet_apache_zones.sh ] ; then\n                    sleep 3 && \/root\/scripts\/SERVERDNS\/update_localnet_apache_zones.sh renewal eth0\n            fi\n" > /tmp/jtemp
                  }
            fi
	else die "cannot find our place in $target/etc/init.d/bind9!"
        fi
    }
else die "$target/etc/init.d/bind9 not found!"
fi
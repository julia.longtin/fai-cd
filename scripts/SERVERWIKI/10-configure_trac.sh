#! /bin/sh

if [ -e "$target${TRAC_ENV}" -a ! -d "$target${TRAC_ENV}" ]; then
        printf '%s exists and is not a directory, so it can not be a trac environment.' "${TRAC_ENV}" >&2
        exit 1
fi

$ROOTCMD trac-admin "${TRAC_ENV}" initenv 'WikiServer' "sqlite:db/trac.db" 'svn' ''

# Until permissions get in, let anyone do anything.
# *** REMOVE THIS BEFORE GOING INTO PRODUCTION! ***
$ROOTCMD trac-admin "${TRAC_ENV}" permission add authenticated TRAC_ADMIN

# Apache owns the progress and will need to be able to fiddle with the TRAC_ENV
$ROOTCMD chgrp www-data -R "${TRAC_ENV}/db"
$ROOTCMD chmod g+s "${TRAC_ENV}/db"
$ROOTCMD chmod g+rw -R "${TRAC_ENV}/db"

# Apache owns the configuration
$ROOTCMD chown www-data.www-data -R "${TRAC_ENV}/conf"

# add an admin user
$ROOTCMD htpasswd -bc "${TRAC_ENV}/trac.htpasswd" admin $TRACPW
$ROOTCMD trac-admin "${TRAC_ENV}" permission add admin         TRAC_ADMIN

fcopy -Bv /etc/apache2/sites-available/trac.conf

$ROOTCMD a2ensite trac
$ROOTCMD a2enmod alias
$ROOTCMD a2enmod fcgid

$ROOTCMD ln -s /usr/share/doc/trac/contrib/cgi-bin /var/www/trac/cgi-bin
$ROOTCMD chmod 755 /usr/share/doc/trac/contrib/cgi-bin/trac.fcgi
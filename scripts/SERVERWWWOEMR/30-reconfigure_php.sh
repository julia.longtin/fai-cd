#!/bin/sh

# turn up two of the defaults, per OpenEMR's recomendation.

cat $target/etc/php5/apache2/php.ini | sed "s/^\(max_execution_time =\) 30$/\1 60/" | sed "s/^\(max_input_time =\) 60/\1 90/" > /tmp/jtemp
cat /tmp/jtemp > $target/etc/php5/apache2/php.ini
#!/bin/sh

# copy in a script that should check out openemr the first boot.
fcopy /etc/init.d/setup_openemr

# register it with the init system.
$ROOTCMD insserv -d setup_openemr
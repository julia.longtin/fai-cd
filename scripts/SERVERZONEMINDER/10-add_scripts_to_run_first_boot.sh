#!/bin/sh

# copy in a script that should set up zoneminder.
fcopy /etc/init.d/setup_zoneminder

# register it with the init system.
$ROOTCMD insserv -d setup_zoneminder
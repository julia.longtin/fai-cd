#!/bin/bash
# 20-make_exim_use_clamav.sh
#
# drop-in replacement for 20-make_exim_use_clamav script.

## Our "die" function (think perl)
function die () { echo "$@" 1>&2 ; exit 1 ; }

[ ! -n "$target" ] && die "variable \$target not defined!"
if [ -f $target/etc/exim4/conf.d/main/01_exim4-config_listmacrosdefs ]; then
    {
        if [ ! -n "`grep \"av_scanner =\" $target/etc/exim4/conf.d/main/01_exim4-config_listmacrosdefs`" ]; then
            echo "av_scanner = clamd:/var/run/clamav/clamd.ctl" >> $target/etc/exim4/conf.d/main/01_exim4-config_listmacrosdefs
        fi
        if [ ! -n "`grep \"TEERGRUBE\" $target/etc/exim4/conf.d/main/01_exim4-config_listmacrosdefs`" ]; then
            echo "TEERGRUBE = 60s" >> $target/etc/exim4/conf.d/main/01_exim4-config_listmacrosdefs
        fi
    }
else
die "$target/etc/exim4/conf.d/main/01_exim4-config_listmacrosdefs not found!"
fi
#!/bin/bash
# 30-enable_spamassassin.sh
#
# drop-in replacement for 30-enable_spamassassin script. 

## Our "die" function (think perl)
function die () { echo "$@" 1>&2 ; exit 1 ; }

[ ! -n "$target" ] && die "variable \$target not defined!"
if [ -f $target/etc/default/spamassassin ]; then
    {
        cat $target/etc/default/spamassassin | sed "s/ENABLED=.*/ENABLED=1/" > /tmp/jtemp
        cat /tmp/jtemp > $target/etc/default/spamassassin
    }
else die "$target/etc/default/spamassassin not found!"
fi


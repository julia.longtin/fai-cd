#!/bin/bash
# Copyright (c) 2002, 2003, 2004 Mark Suter <suter@humbug.org.au>
#
# Modifications Copyright (c) 2005 Justin Doiel <ytiddo@volumehost.com>
#
# This shell script uses utilities shipped with ISC BIND to
# update a zone using Dynamic DNS Update requests (RFC2136).
#
# This paticular one is written to automatically update "blah.com" records for the local network to point at the newly assigned DHCP address.
# EG: if your webserver is on DHCP, and your firewall has a static, and is port forwarding to the webserver and you want the internal network interface properly represented by this DNS server.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# $Id: dynamic-dns-update,v 1.26 2004/05/17 02:28:46 suter Exp $

################################
##  Start User Configuration  ##
################################

#accepts two arguments
# interface to update information on
# and the DNS hostname we need to update.
# FIXME: reverse DNS?


## The interface who's address we want published
localnet=$1

## The Time To Live we should use
ttl=$(( 60 * 30 ))

## The hostname we're doing dynamic updates on
hostname=$2

## IP address of DNS server to update
#dnsserver=127.0.0.1
# can't use 127.0.0.1, cause we've got views turned on, and we're updating the localnet interface.
#keyname=$hostname		## Whatever the server expects
#key="TG6KQ9zBEzvGcsWKRO96zA=="  ## Shared key (no files needed)
# establish where bind is listening, on eth0
firstoctet=`ip addr show eth0 | sed -n "s/.*inet \([0-9]*\).*/\1/p"`
firstthreeoctets=`ip addr show eth0 | sed -n "s/.*inet \([0-9.]*\)\..*/\1/p"`

bind=`netstat -anp | grep named | sed -n "s/[ut][dc]p *[0-9]* *[0-9]* $firstoctet\([0-9.]*\).*/$firstoctet\1/p"`

if [ "x$bind" == "x" ] ; then
    {
	if [ "x`pgrep named`" == "x" ] ; then
	    {
		echo "bind not started, doing nothing."
		exit 0
	    }
	else
	    {
		echo "bind is not listening, restarting..."
		/etc/init.d/bind9 restart
	    }
	fi
    }
fi


dnsserver=`ip addr show eth0 | sed -n "s/.*inet \([0-9.]*\).*/\1/p"`
keyname=`cat /etc/bind/rndc.key | grep key | sed "s/key \"\(.*\)\".*/\1/"`
key=`cat /etc/bind/rndc.key | grep secret | sed "s/.*secret \"\(.*\)\".*/\1/"`

################################
##   Bind v9 Documentation    ##
################################

# This key was generated using bind9's dnssec-keygen(8) utility,
# for example,
#
#    $ dnssec-keygen -a HMAC-MD5 -b 128 -n host host.example.org
#    $ cat Khost.example.org.*.key
#    host.example.org. IN KEY 512 3 157 TG6KQ9zBEzvGcsWKRO96zA==
#
# Here's a corresponding configuration for ISC BIND 9:
#
#     key "host.example.org" {
#         algorithm hmac-md5;
#         secret "wJrOTT0umFTIllqV1Xk8sQ==";
#     };
#
#     zone "example.org" {
#         ...
#         update-policy { grant * self - A; };
#     };
#
# More information is available in the Administrator Reference Manual
# shipped with BIND 9 and available from http://www.isc.org/sw/bind/

################################
##   End User Configuration   ##
################################

## Our "die" function (think perl)
function die () { echo "$@" 1>&2 ; exit 1 ; }

## Test for needed binaries in the PATH
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
hash ping cat host ifconfig nsupdate tail perl test || die $0: required binaries not present

## Make sure we can reach the nameserver
ping -c 1 $dnsserver >/dev/null 2>&1 || die $0: could not ping $dnsserver

## Get the old and new IP addresses
#old=$(host $hostname $dnsserver | tail -1 | perl -ane'print $F[-1]')
#new=$(ifconfig $internet 2>&1 | perl -ne'/inet addr:(\S+)/ and print $1')
old=$(host $hostname $dnsserver | grep address | tail -1 | perl -ane'print $F[-1]')
new=$(ifconfig $localnet 2>&1 | perl -ne'/inet addr:(\S+)/ and print $1')

## Simple check that we have a new address
test -n "$new" || die $0: could not find IP address

## Ensure there is a change to publish
test "${new}" != "${old}" || exit 0

## Invoke the correct nsupdate(8), using process substitution to avoid a temporary file

echo -n "updating $hostname on $dnsserver to point to $new.."

## Bind 9
nsupdate <( cat <<-BIND9
key ${keyname} ${key}
server ${dnsserver}
update delete ${hostname} A
update add ${hostname} ${ttl} IN A ${new}
send
BIND9
) || die $0: nsupdate failed.

exit 0
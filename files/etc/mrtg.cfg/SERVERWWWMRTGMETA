######################################################################
# Multi Router Traffic Grapher -- Sample Configuration File
######################################################################
# This file is for use with mrtg-2.5.4c

# Global configuration
WorkDir: /srv/www/mrtg
WriteExpires: Yes

# our section
#RunAsDaemon: Yes
Options[_]:bits, growright
# no_cpus * 2
Forks: 4

### Global Config Options

#  for UNIX
# WorkDir: /home/http/mrtg

#  for Debian
WorkDir: /srv/www/mrtg

#  or for NT
# WorkDir: c:\mrtgdata

### Global Defaults

#  to get bits instead of bytes and graphs growing to the right
# Options[_]: growright, bits

EnableIPv6: yes

######################################################################
# System: metahost
# Description: createvm dev box
# Contact: Root <root@localhost> (configure /etc/snmp/snmpd.local.conf)
# Location: Unknown (configure /etc/snmp/snmpd.local.conf)
######################################################################

# numeric OIDs:
# 1.3.6.1.4.1.2021.11.50.0 == ssCpuRawUser
# 1.3.6.1.4.1.2021.11.52.0 == ssCpuRawSystem
# 1.3.6.1.4.1.2021.11.51.0 == ssCpuRawNice
# 1.3.6.1.4.1.2021.11.53.0 == ssCpuRawIdle

# CPU Usage Graph
Target[localhost.cpu]:1.3.6.1.4.1.2021.11.50.0&1.3.6.1.4.1.2021.11.50.0:zoneone@localhost + 1.3.6.1.4.1.2021.11.52.0&1.3.6.1.4.1.2021.11.52.0:zoneone@localhost + 1.3.6.1.4.1.2021.11.51.0&1.3.6.1.4.1.2021.11.51.0:zoneone@localhost 
RouterUptime[localhost.cpu]: zoneone@localhost
MaxBytes[localhost.cpu]: 1000
Title[localhost.cpu]: CPU Usage
PageTop[localhost.cpu]: <H1> CPU Usage </H1>
ShortLegend[localhost.cpu]: %
Ylegend[localhost.cpu]: CPU Usage
Legend1[localhost.cpu]: CPU Usage in %
Legend2[localhost.cpu]: 
Legend3[localhost.cpu]: 
Legend4[localhost.cpu]: 
LegendI[localhost.cpu]: Usage
LegendO[localhost.cpu]: 
Options[localhost.cpu]: growright, nopercent

# Load Average Graph
Target[localhost.load]:1.3.6.1.4.1.2021.10.1.5.2&1.3.6.1.4.1.2021.10.1.5.3:zoneone@localhost 
RouterUptime[localhost.load]: zoneone@localhost
MaxBytes[localhost.load]: 5000
Title[localhost.load]: Load Average * 100
PageTop[localhost.load]: <H1> Load Average * 100 </H1>
ShortLegend[localhost.load]: 
Ylegend[localhost.load]: Load Average
Legend1[localhost.load]: Load average 5 min
Legend2[localhost.load]: Load average 15 min
Legend3[localhost.load]: 
Legend4[localhost.load]: 
LegendI[localhost.load]: 5min load avg
LegendO[localhost.load]: 15min load avg
Options[localhost.load]: growright, nopercent, gauge

# Free Memory Graph
Target[localhost.mem]: 1.3.6.1.4.1.2021.4.6.0&1.3.6.1.4.1.2021.4.4.0:zoneone@localhost * 1024
MaxBytes[localhost.mem]: 10000000000
Title[localhost.mem]: Free Memory
PageTop[localhost.mem]: <H1> Free Memory/Swap </H1>
ShortLegend[localhost.mem]: bytes
Ylegend[localhost.mem]: bytes
Legend1[localhost.mem]: Free memory / swap space, in bytes
Legend2[localhost.mem]: 
Legend3[localhost.mem]: 
Legend4[localhost.mem]: 
LegendI[localhost.mem]: Free memory:
LegendO[localhost.mem]: Free swap:
Options[localhost.mem]: growright, nopercent, gauge

# total - buffers - cache - free

Target[localhost.memrealused]: ( 1.3.6.1.4.1.2021.4.5.0&1.3.6.1.4.1.2021.4.5.0:zoneone@localhost - 1.3.6.1.4.1.2021.4.14.0&1.3.6.1.4.1.2021.4.14.0:zoneone@localhost - 1.3.6.1.4.1.2021.4.15.0&1.3.6.1.4.1.2021.4.15.0:zoneone@localhost - 1.3.6.1.4.1.2021.4.6.0&1.3.6.1.4.1.2021.4.6.0:zoneone@localhost ) * 1024
MaxBytes[localhost.memrealused]: 1000000000
Title[localhost.memrealused]: Used Memory
PageTop[localhost.memrealused]: <H1> Used Memory </H1>
ShortLegend[localhost.memrealused]: bytes
Ylegend[localhost.memrealused]: bytes
Legend1[localhost.memrealused]: Used memory, not including swap space, in bytes
Legend2[localhost.memrealused]: 
Legend3[localhost.memrealused]: 
Legend4[localhost.memrealused]: 
LegendI[localhost.memrealused]: Used memory:
LegendO[localhost.memrealused]: 
Options[localhost.memrealused]: growright, nopercent, gauge

Target[localhost.memused]: 1.3.6.1.4.1.2021.4.14.0&1.3.6.1.4.1.2021.4.15.0:zoneone@localhost  * 1024
MaxBytes[localhost.memused]: 1000000000
Title[localhost.memused]: Buffers and Cache
PageTop[localhost.memused]: <H1> Buffers and Cache </H1>
ShortLegend[localhost.memused]: bytes
Ylegend[localhost.memused]: bytes
Legend1[localhost.memused]: Memory used for Buffers or Cache
Legend2[localhost.memused]: 
Legend3[localhost.memused]: 
Legend4[localhost.memused]: 
LegendI[localhost.memused]: Disk Buffers:
LegendO[localhost.memused]: Cache:
Options[localhost.memused]: growright, nopercent, gauge

# ping default route
Target[localhost.ping-inet]: `/etc/mrtg/ping_internet.sh`
MaxBytes[localhost.ping-inet]: 10000
Title[localhost.ping-inet]: Ping Time to Internet
PageTop[localhost.ping-inet]: <H1> Ping Time to Internet</H1>
ShortLegend[localhost.ping-inet]: %
Ylegend[localhost.ping-inet]: RTT (ms)
Legend1[localhost.ping-inet]: 
Legend2[localhost.ping-inet]: 
Legend3[localhost.ping-inet]: 
Legend4[localhost.ping-inet]: 
LegendI[localhost.ping-inet]: Pkt loss +
LegendO[localhost.ping-inet]: Avg RTT
Options[localhost.ping-inet]: growright, unknaszero, nopercent, gauge, noinfo

### Interface 2 >> Descr: 'Public' | Name: 'public'

Target[localhost_2]: 1:zoneone@localhost:
SetEnv[localhost_2]: MRTG_INT_DESCR="public"
MaxBytes[localhost_2]: 1250000
Title[localhost_2]: Traffic on Public Interface
PageTop[localhost_2]: <h1>Traffic on Public Interface</h1>
		<div id="sysdetails">
			<table>
				<tr>
					<td>System:</td>
					<td>Unknown</td>
				</tr>
				<tr>
					<td>Maintainer:</td>
					<td>Root &lt;root@localhost&gt; </td>
				</tr>
				<tr>
					<td>Description:</td>
					<td>Public Interface</td>
				</tr>
				<tr>
					<td>ifType:</td>
					<td>ethernetCsmacd (6)</td>
				</tr>
				<tr>
					<td>ifName:</td>
					<td>public</td>
				</tr>
				<tr>
					<td>Max Speed:</td>
					<td>1250.0 kBytes/s</td>
				</tr>
			</table>
		</div>
ShortLegend[localhost_2]: bytes
Ylegend[localhost_2]: bytes
Legend1[localhost_2]: Traffic on Public Interface
Legend2[localhost_2]: 
Legend3[localhost_2]: 
Legend4[localhost_2]: 
LegendI[localhost_2]: In:
LegendO[localhost_2]: Out:
Options[localhost_2]: growright, nopercent, gauge

